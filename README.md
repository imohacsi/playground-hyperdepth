Copyright (c) 2018 Istvan Mohacsi (imohacsi@gmail.com)

All rights reserved.

This code provides a simplified implementation of the Hyperdepth paper (Fanello et al., 2016) using only a few lines of the image and no subpixel regression. Also it randomly selects features instead of learning them line-by-line.


==Instructions to run==

A few first notes:
The implementation was done in Python, so no tests were made regarding speed.
I haven't added padding to the codes, so the active field is reduced.
Also there are no safety checks for bad parameter values.

===Dependencies===
Non-default python packages: sklearn,cv2,pickle

===Step 1: preparing ground truth data===
Script file: ground_truth_generator.py

The first step is to generate the ground truth data based on the set of provided images and the corresponding reference image. Based on your recommendation, I used OpenCV's block matching for ground truth data generation. The script writes the generated displacement maps into 16 bit PNG files for training.
Parameters:
refr_path : Direct file path for the reference pattern
data_path : Folder path that contains the infrared images
save_path : Output folder path to store the displacement maps as 16 bit PNG images

Hyperharameters:
max_displacement : Maximum displacement from block matching (default: 128)
block_size : Size of the matched blocks (default: 23)
The hyperparameters were chosen to yield visually good accuracy and high coverage.


===Step 2: Perform the training===
Script file: train_model.py or train_model_memsaver.py*
*In case of a large number of lines (20+).

Run it by:
python3 train_model_memsaver.py 

The actual training step to train a separate classifier for each line to predict the location as discrete classes followed by strong filtering based on the data, consistency, confidence, etc... In my implementation, the "Pixel-Difference Test" features were pre-calculated using a set of random offsets (sampled from r=15px around the center pixel). These offsets could be pruned based on their importance to save on resources and speed. My code is equipped to use the RandomForest classifier as MLP or SVM takes awful long time to train. No extensive hyperparameter optimization has taken place. Following the classifier, the predictions are filtered based on pre-defined criteria. After training each line, the classifiers are evaluated on a subset of train and test data to evaluate their accuracy. Finally, a short movie of the reconstructed disparity maps is shown (nice if you have many lines) and some images are saved.

Filter hyperparameters are hardcoded in fconfig.py. They are more forgiving than in the original paper, so overall accuracy is not so high, but it produces less gaps.

Parameters:
data_path : Folder path that contains the infrared images
labl_path : Folder path that contains the displacement maps as 16 bit PNG images
do_filter_gt : True to reduce training data

num_threads : Number of threads if the classifier supports it
num_pixels_x : Width of images
num_pixels_y : Height of images

Extraction hyperparameters:
split_frac : Train-Test split ratio
num_features : Number of random features to be extracted
pixel_radius : Pixel radius for random features
line_indices : Numpy array with the line indices, have "pixel_radius" margin!

Model hyperparameters:
forest_depth : Random forest depth
forest_max_features : Random forest complexity
forest_estimators : Random forest ensemble size


==Quick results==

Due to the number of classes, the minimum required depth is 11.
Accuracy: Represents the correctly identified positions in the filtered data
Valid_px: Represents the fraction of non-filtered positions (i.e. good enough)
Acc_cor:  Represents the correctly identified portion of the unfiltered data (this is the final evaluation metric)

A gallery of some of the more detailed depth-map and image pairs can be found in the "images" subfolder.
These were made with 40 lines, estimators=10, max_features=8, depth=13. Accuracy metrics were: (0.8039	0.8593	0.6908)
We did not apply cosmetic filters or interpolation on the produced displacement maps.
We only performed line searches with single hyperparameters, no complex tuning.

===Location and data quality===

As a general observation, lines around the middle of the detector seemed to have slightly worse signal quality, but this can also be a sampling effect due to larger distances and reflective or dark surfaces being more frequent.

===Number of forests===
Average over 10 lines and 10000 samples per line. Error bars are ~0.02, so negligible.
N	Acc_te	Valid_px	Accorr
1	0.5019	0.4154	0.2085
2	0.6000	0.5133	0.3080
4	0.6968	0.6195	0.4317
8	0.7782	0.6776	0.5273
12	0.8002	0.6560	0.5249
16	0.8242	0.6054	0.4990
24	0.8427	0.5136	0.4328
32	0.8657	0.4537	0.3928

As additional trees get added the classifier learns to no longer make small errors. Its classification becomes binary: either correct or very off. According to our studies, 10 classifiers will form a reasonable ensemble for our further studies. 

===Tree depth===

10	0.7778	0.3451	0.2684
11	0.7907	0.5370	0.4246
12	0.7867	0.6601	0.5193
13	0.7894	0.7398	0.5840
14	0.7957	0.7955	0.6329
15	0.8043	0.8219	0.6610

More complex trees bring noticeable benefits at the cost of exploding CPU and RAM requirements. A depth of 12 sounds a good choice for speed, while 13 for accuracy. Higher values are hard to train and store.

===Tree max_features==
1	0.7834	0.6687	0.5238
2	0.7852	0.7385	0.5799
4	0.7840	0.7807	0.6121
8	0.7877	0.8157	0.6425
16	0.7901	0.8492	0.6710
32	0.7810	0.8521	0.6655
The more the better and they don't seem to increase memory consumption so much. Above 4, we start to hit the diminishing return.

===Number of random features===
These can be automatically selected and pruned, but with a random guess we always took 128 features for testing. The results become noticeably better or worse with more/less samples, but its getting too much. 

===Filters===
Applying stricter filtering greatly reduces the localization (RMS) error and also raises accuracy. However it also greatly reduces the number of pixels where valid predictions could be made. We used much lighter thresholding criteria than the original authors to preserve a higher fraction of pixels for our predictions. 



