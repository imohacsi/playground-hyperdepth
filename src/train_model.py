#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 00:57:23 2018

@author: imohacsi
"""

import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
import pickle
import pandas as pd
import numpy as np
import cv2

import utilities as  utils
import evaluation as evals
import fconfig as    fconf

data_path = "../ads/IR";
labl_path = "../ads/GT";
do_filter_gt = True;            #True if you dont trust it
do_testwrite =  True;

num_threads= 2;
num_pixels_x = 1280;            #Line length in images
num_features = 64;              #Number of random features to be extracted
pixel_radius = 15;              #Pixel radius for random features
split_frac = 0.8;               #Train-Test split ratio
forest_depth = 12;              #Random forest depth
forest_max_features = 1;        #Random forest complexity
forest_estimators = 4;         #Random forest ensemble size

filters = fconf.fconf();        #Filter config, use default settings
line_indices = np.arange( 32,992,96 );

""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""
"""Pre calculate a random number of line displacements"""
disp_idx = utils.generate_displacements( num_features, pixel_radius );

"""Get list of IR image filenames"""
imglist = utils.list_images( data_path );
imglist = imglist.sample( n=800 );
imglist = imglist.reset_index(drop=True);
"""Extract features from images"""
afvec,advec,apvec,aivec = utils.load_images( imglist, data_path, labl_path, line_indices, disp_idx, 
                                        pixel_radius, num_pixels_x-2*pixel_radius );
print("Done with loading images.")

"""Set up random forest classifiers"""
models = []
for x in range(len(line_indices)):
#    models.append( MLPClassifier( hidden_layer_sizes=(8,8,8), activation='relu', random_state=123, verbose=1 ) );    
#    models.append( SVC( random_state=123, verbose=0 ) );    
    models.append( RandomForestClassifier( n_estimators=forest_estimators, max_features=forest_max_features, max_depth=forest_depth, 
                                          criterion='entropy', random_state=123, verbose=0, n_jobs=num_threads ) );

acc_tr  = [0]*len(line_indices);
acc_te  = [0]*len(line_indices);
rmse_tr = [0]*len(line_indices);
rmse_te = [0]*len(line_indices);
f_kept  = [0]*len(line_indices);
"""Looping through lines"""
for lidx in range( len(line_indices)):   
    print("Calculating line %d of %d" % (lidx,len(line_indices)) );
#    print("\tPreparing data...")
    df_features = utils.prepare_dataframes(afvec[lidx,:,:,:],advec[lidx,:,:],apvec[lidx,:,:],aivec[lidx,:,:],filters,do_filter_gt );                                                   

    #Perform train-test split
    df_train = df_features.sample( frac=split_frac );
    df_test = df_features.drop( df_train.index );
    del df_features;
    
    #Fit the model
#    print("\tFitting model...")
    models[lidx].fit( df_train.drop(['labels','xcoord','displc','totint'],1), df_train['labels'] );

    #Calculate accuracy
#    print("\tRunning diagnostics...")
    tmp = evals.eval_accuracy(df_train,df_test, models[lidx], filters, verbose=False );
    acc_tr[lidx],acc_te[lidx],rmse_tr[lidx],rmse_te[lidx],f_kept[lidx] = tmp;
    del df_train,df_test;

print("Summarized statistics:")
print("Train accuracy: \t%0.4g +/- %g" % (np.mean(acc_tr),np.std(acc_tr)) );
print("Test  accuracy: \t%0.4g +/- %g"  % (np.mean(acc_te),np.std(acc_te)) );
print("Train RMS err:  \t%0.4g +/- %g" % (np.mean(rmse_tr),np.std(rmse_tr)) );
print("Test RMS err:   \t%0.4g +/- %g"  % (np.mean(rmse_te),np.std(rmse_te)) );
print("Valid pixels:   \t%0.4g +/- %g" % (np.mean(f_kept),np.std(f_kept)) );

"""Memory cleanup"""
del afvec,advec,apvec,aivec;

#"""Saving models (for new python versions)"""
#pickle.dump(models, open('HyperDepth_classifiers.pkl', 'wb'), -1 );   

print("Popping up some example images...")
sample_names  = imglist.sample(n=100).reset_index(drop=True);
evals.pop_examples( models,sample_names,data_path,line_indices,disp_idx, 
                   pixel_radius, num_pixels_x, filters, do_write=do_testwrite);


