# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 17:08:10 2018

@author: mohacsii
"""
import pandas as pd
import numpy as np

class fconf:
    def __init__(self):
        self.strict();
        
    def strict(self):
        self.int_min = 5000;
        self.dist_max = 10;
        self.prob_min = 0.05;
        self.disp_min= 10;
        self.disp_max= 128+8;

    def unset(self):
        self.int_min = 0;
        self.dist_max = 500000000;
        self.prob_min = 0.0;
        self.disp_min= 1;
        self.disp_max= 160000000;

    def loose(self):
        self.int_min = 5000;
        self.dist_max = 50;
        self.prob_min = 0.01;
        self.disp_min= 5;
        self.disp_max= 160;
    
    #Filtering predictions    
    def apply_pd( self, df ):
        #Distance of top 2 predictions
        try:
            df = df[ np.abs(df['top1']-df['top2'])< self.dist_max ];
        except:
            pass;
        
        #Confidence of main prediction
        df = df[ df['confidence']>self.prob_min];
            
        #Minimum intensity in region
        df = df[ df['totint']>self.int_min];

        #Displacement within bounds
        df = df[ (df['xcoord']-df['prediction'])>self.disp_min ];
        df = df[ (df['xcoord']-df['prediction'])<self.disp_max ];
        
        return df;
        
    
    #Filtering predictions    
    def apply_np( self, predictions, probabilities=None, xvec=None, ivec=None ):
        if probabilities is not None:
            #Distance of top 2 predictions
            assort = np.argsort( probabilities, axis=-1 );
            dist = np.abs(assort[:,1]-assort[:,2]);
            predictions[dist>self.dist_max] = -1;
            #Confidence of main prediction
            conf = np.max( probabilities, axis=-1 );            
            predictions[conf<self.prob_min] = -1;

        if xvec is not None:
            displacement = xvec - predictions;
            predictions[displacement<self.disp_min] = -1; 
            predictions[displacement>self.disp_max] = -1; 
            
        if ivec is not None:
            predictions[ivec<self.int_min] = -1; 
        return predictions;
        
        
        
        