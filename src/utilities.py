#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 00:57:23 2018

@author: imohacsi
"""
import matplotlib.pyplot as plt
import  scipy.ndimage.filters as filters
import pandas as pd
import numpy as np
import os
import cv2

"""Pre-define random displacement vecors"""
"""TODO: Handle zero displacement       """
def generate_displacements( num_features, r_max=15 ):
    line_displacements = [];
    for dd in range( num_features ):
        line_displacements.append( np.random.randint(low=-r_max,high=r_max,size=4) );
    return line_displacements;
    
    
"""Calculate the pixel difference with pre-defined displacements from a single line"""
def extract_line( imgslice, uu, vv, r=15 ):
    sel1 = imgslice[ uu[1]+r, r+uu[0]:(-r+uu[0]) ].astype(np.int16);
    sel2 = imgslice[ vv[1]+r, r+vv[0]:(-r+vv[0]) ].astype(np.int16);
    
    fvec = sel1-sel2;
    return fvec;

"""Calculate the total signal along a line within the sampled window"""
def signal_check( imgslice, r=15 ):
    imgslice = np.float32(imgslice);
    filtered = filters.uniform_filter(imgslice, size=2*r+1, mode='constant')
    fline = filtered[r,r:-r]*np.square(2*r+1);
    return fline;

"""Extract multiple sets of feature vectors from an image at predefined coordinates"""
def extract_image( image, line_idx, displacements, r_max, n_x ):
    #Pre-allocate for extra speed
    i_fvec = np.zeros( [len(line_idx),n_x,len(displacements)], dtype=np.int16 );
    i_pvec = np.zeros( [len(line_idx),n_x], dtype=np.int16);
    i_ivec = np.zeros( [len(line_idx),n_x], dtype=np.float32);
    x_vec = np.arange( n_x );
    
    #WARNING: I was too lazy to pad the images with zeros
    for lidx,ll in enumerate(line_idx):
        roi = image[ll-r_max:ll+r_max+1,:];
        
        for didx,dd in enumerate(displacements):
            i_fvec[lidx,:,didx] = extract_line( roi, [dd[0],dd[1]], [dd[2],dd[3]], r_max );
        i_pvec[lidx,:] = x_vec;
        i_ivec[lidx,:] = signal_check( roi, r_max );
        
    return i_fvec,i_pvec,i_ivec

"""Extract multiple sets of feature vectors from an NP array of images at predefined coordinates"""
def extract_slab( images, dpmaps, lcoord, ldisplacements, r_max, n_x ):
    #Pre-allocate for extra speed
    s_fvec = np.zeros( [images.shape[0],n_x,len(ldisplacements)], dtype=np.int16 );
    s_dvec = np.zeros( [images.shape[0],n_x], dtype=np.int16);
    s_pvec = np.zeros( [images.shape[0],n_x], dtype=np.int16);
    s_ivec = np.zeros( [images.shape[0],n_x], dtype=np.float32);
    x_vec = np.arange( n_x );
    
    #These arent image specific
    s_dvec[:,:] = dpmaps[:,lcoord,r_max:-r_max];
    s_pvec[:,:] = x_vec;
    
    #These are image dependent
    for idx in range(images.shape[0]):
        roi = images[idx,lcoord-r_max:lcoord+r_max+1,:];
        s_ivec[idx,:] = signal_check( roi, r_max );    
        for didx,dd in enumerate(ldisplacements):
            s_fvec[idx,:,didx] = extract_line( roi, [dd[0],dd[1]], [dd[2],dd[3]], r_max );
            
    return s_fvec,s_dvec,s_pvec,s_ivec


"""Extract the raw feature data from a list of image file names (for few features)"""    
def load_images( file_list, img_path, gt_path, line_idx, displacements, r_max, n_x ):
    #Pre-allocate for extra speed
    fvec_list = np.zeros( [len(line_idx),len(file_list),n_x,len(displacements)], dtype=np.int16 );
    dvec_list = np.zeros( [len(line_idx),len(file_list),n_x], dtype=np.int16);
    pvec_list = np.zeros( [len(line_idx),len(file_list),n_x], dtype=np.int16);
    ivec_list = np.zeros( [len(line_idx),len(file_list),n_x], dtype=np.float32);
   
    #Run through all images
    for idx,entr in file_list.iterrows():
        if idx%100==0:
            print( "Reading image ", idx )
        #Open and load images
        imag_name = "%s/%s.png" % (img_path,entr['hash']);
        disp_name = "%s/%s.png" % (gt_path, entr['hash']);
        
        image = cv2.imread( imag_name, 0 );
        dpmap = cv2.imread( disp_name, -1 )/16;       #OpenCV automatically multiplies it by 16    
        
        #This we extract separately
        for lidx,ll in enumerate(line_idx):
            dvec_list[lidx,idx,:] = dpmap[ll,r_max:-r_max];
            
        tmp = extract_image( image, line_idx, displacements, r_max, n_x );
        fvec_list[:,idx,:,:] = tmp[0];
        pvec_list[:,idx,:]   = tmp[1];
        ivec_list[:,idx,:]   = tmp[2];

    return fvec_list,dvec_list,pvec_list,ivec_list;
    
"""Reads images into memory as NP arrays"""    
def read_images( file_list, img_path ):
    #Pre-allocate for extra speed
    image_list = [];
    #Run through all images
    for idx,entr in file_list.iterrows():
        if idx%100==0:
            print( "Reading image ", idx )
        #Open and load images
        imag_name = "%s/%s.png" % (img_path,entr['hash']);
        
        image = cv2.imread( imag_name, 0 );
        image_list.append( image );

    return image_list;

"""Reads images into memory as NP arrays"""    
def read_pairs( file_list, img_path, gt_path, n_x, n_y ):
    #Pre-allocate for extra speed
    image_list = np.zeros([len(file_list),n_y,n_x], dtype=np.uint8);
    dpmap_list = np.zeros([len(file_list),n_y,n_x], dtype=np.uint8);
    #Run through all images
    for idx,entr in file_list.iterrows():
        if idx%100==0:
            print( "Reading image ", idx )
        #Open and load images
        imag_name = "%s/%s.png" % (img_path,entr['hash']);
        disp_name = "%s/%s.png" % (gt_path, entr['hash']);
        
        image_list[idx,:,:] = cv2.imread( imag_name, 0 );
        dpmap_list[idx,:,:] = cv2.imread( disp_name, -1 )/16;       #OpenCV automatically multiplies it by 16    
    return image_list,dpmap_list;

"""Lists all images in a directory"""
def list_images( image_directory ):
    #List all supported image files in folder    
    filelist= os.listdir( image_directory );
    image_file_list = [ fil for fil in filelist if ( fil.endswith(".jpg") or fil.endswith(".JPEG") or fil.endswith(".png") or fil.endswith(".PNG") ) ];

    #Generate full entry and append to list
    list_of_entries = [];
    for file in image_file_list:
        fullname = image_directory+'/'+file;
        filename = os.path.splitext( file )[0];
        list_of_entries.append( list([fullname, filename]) );
        
    fileDFrame = pd.DataFrame( list_of_entries, columns=['filename','hash']);
    return fileDFrame;

"""Prepare dataframe from raw array data"""
def prepare_dataframes( fvec, dvec, pvec, ivec, filters, do_filter_gt ):
    
    df_features = pd.DataFrame( np.reshape( fvec, [-1, fvec.shape[2]] ) );
    df_features['displc'] = np.reshape( dvec, [-1] );
    df_features['xcoord'] = np.reshape( pvec, [-1] );
    df_features['totint'] = np.reshape( ivec, [-1] );

    #First pre-filtering
    df_features = df_features[ df_features['displc']>0 ];   #Where displacement could not be determined

    if do_filter_gt:
        df_features = df_features[ df_features['displc']>filters.disp_min ]; #Where it appears too close
        df_features = df_features[ df_features['displc']<filters.disp_max ]; #Where it appears too far
        df_features = df_features[ df_features['totint']>filters.int_min  ]; #Where there wasnt enough intensity
    df_features['labels'] = df_features['xcoord'] - df_features['displc'];
    
    return df_features;
