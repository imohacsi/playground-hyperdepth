#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 00:57:23 2018

Implements the first stage of Hyperdepth using an alternative caching algorithm that is more suited for large number of lines.

@author: imohacsi
"""

import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np
import cv2
import pickle

import utilities as  utils
import evaluation as evals
import fconfig as    fconf

data_path = "../ads/IR";
labl_path = "../ads/GT";
do_filter_gt = True;            #True if you dont trust it
do_testwrite = True;            #True to write a few images at the end

num_threads= 6;
num_pixels_x = 1280;            #Image width
num_pixels_y = 1024;            #Image height
num_features = 128;              #Number of random features to be extracted
pixel_radius = 15;              #Pixel radius for random features
split_frac = 0.8;               #Train-Test split ratio
forest_depth = 12;              #Random forest depth
forest_max_features = 4;        #Random forest complexity
forest_estimators = 10;         #Random forest ensemble size

filters = fconf.fconf();        #Filter config, use default settings
line_indices = np.arange( 32,992,32 );

""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""

disp_idx = utils.generate_displacements( num_features, pixel_radius );

"""Get list of IR image filenames"""
imglist = utils.list_images( data_path );
im_train = imglist.sample( frac=split_frac );
im_test = imglist.drop( im_train.index );
im_train = im_train.reset_index(drop=True);
im_test = im_test.reset_index(drop=True);

"""Extract features from images"""
im_tr,dp_tr = utils.read_pairs( im_train, data_path, labl_path, num_pixels_x, num_pixels_y );
im_te,dp_te = utils.read_pairs( im_test,  data_path, labl_path, num_pixels_x, num_pixels_y );
print("Done with loading images.")

#"""Set up random forest classifiers"""
models = []
for x in range(len(line_indices)):
    models.append( RandomForestClassifier( n_estimators=forest_estimators, max_features=forest_max_features, 
                                          max_depth=forest_depth, criterion='entropy', random_state=123, 
                                          verbose=0, n_jobs=num_threads ) ); 
acc_tr  = [0]*len(line_indices);
acc_te  = [0]*len(line_indices);
rmse_tr = [0]*len(line_indices);
rmse_te = [0]*len(line_indices);
f_kept  = [0]*len(line_indices);
"""Looping through lines"""
for lidx in range( len(line_indices)):   
    print("Calculating line %d of %d" % (lidx,len(line_indices)) );
    print("\tPreparing data...")
    #Extract line features from slabs
    fvec_tr,dvec_tr,pvec_tr,ivec_tr = utils.extract_slab( im_tr, dp_tr, line_indices[lidx], disp_idx, 
                                                         pixel_radius, num_pixels_x-2*pixel_radius );
    fvec_te,dvec_te,pvec_te,ivec_te = utils.extract_slab( im_te, dp_te, line_indices[lidx], disp_idx, 
                                                         pixel_radius, num_pixels_x-2*pixel_radius );
    #Transform features to PD dataframes and filter them                                        
    tr_features = utils.prepare_dataframes(fvec_tr,dvec_tr,pvec_tr,ivec_tr,filters,do_filter_gt );                                                   
    te_features = utils.prepare_dataframes(fvec_te,dvec_te,pvec_te,ivec_te,filters,do_filter_gt );                                                   
    del fvec_tr,dvec_tr,pvec_tr,ivec_tr;
    del fvec_te,dvec_te,pvec_te,ivec_te;

    #Fit the model
    print("\tFitting model...")
    models[lidx].fit( tr_features.drop(['labels','xcoord','displc','totint'],1), tr_features['labels'] );

    #Calculate accuracy
    tmp = evals.eval_accuracy(tr_features,te_features, models[lidx], filters );
    acc_tr[lidx],acc_te[lidx],rmse_tr[lidx],rmse_te[lidx],f_kept[lidx] = tmp;
    del tr_features,te_features;

print("Summarized statistics:")
print("Train accuracy: \t%0.4g +/- %g" % (np.mean(acc_tr),np.std(acc_tr)) );
print("Test  accuracy: \t%0.4g +/- %g"  % (np.mean(acc_te),np.std(acc_te)) );
print("Train RMS err:  \t%0.4g +/- %g" % (np.mean(rmse_tr),np.std(rmse_tr)) );
print("Test RMS err:   \t%0.4g +/- %g"  % (np.mean(rmse_te),np.std(rmse_te)) );
print("Valid pixels:   \t%0.4g +/- %g" % (np.mean(f_kept),np.std(f_kept)) );

"""Memory cleanup"""
del im_tr,im_te,dp_tr,dp_te;

#"""Saving models (for new python versions) - Brutal RAM requirement"""
#pickle.dump(models, open('HyperDepth_classifiers.pkl', 'wb'), -1 );   


print("Popping up some example images...")
sample_names  = imglist.sample(n=100).reset_index(drop=True);
evals.pop_examples( models,sample_names,data_path,line_indices,disp_idx, 
                   pixel_radius, num_pixels_x, filters, do_write=do_testwrite);


