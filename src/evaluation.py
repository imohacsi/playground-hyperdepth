#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  9 00:57:23 2018

@author: imohacsi
"""
import matplotlib.pyplot as plt
import numpy as np
import os, cv2

import utilities as utils

def accuracy( labels, logits ):
    return np.mean(labels==logits);
    
def mse( labels, logits ):
    return np.mean( np.square(np.float64(labels)-np.float64(logits)) );

def rmse( labels, logits ):
    return np.sqrt(mse(labels, logits));

def eval_accuracy(df_train,df_test, model, fconfig, num_points=10000, verbose=True ):
    df,tr_acc,tr_rmse = eval_batch_filtered( df_train.sample(n=num_points), model, fconfig ); 
    df,te_acc,te_rmse = eval_batch_filtered( df_test.sample(n=num_points),  model, fconfig );
    f_kept = float(len(df))/num_points;
    if verbose:
        print( "Accuracy: %f / %f kept %g" % ( tr_acc,  te_acc,  f_kept ) );
        print( "RMSE: %f / %f kept %g" %     ( tr_rmse, te_rmse, f_kept ) );
    return tr_acc,te_acc,tr_rmse,te_rmse,f_kept;   

#Measure prediction accuracy after extensive filtering
def eval_batch_filtered( df, model, fconfig ):
    
    probabilities = model.predict_proba( df.drop(['labels','xcoord','displc','totint'],1) );   
    df['prediction'] = model.predict( df.drop(['labels','xcoord','displc','totint'],1) );    
    df['confidence'] = np.max( probabilities, axis=-1 );    
    
    assort = np.argsort( probabilities, axis=-1 );
    df['top1'] = assort[:,1];
    df['top2'] = assort[:,2];
    del assort,probabilities;
    
    """Beginning the filtering"""
    df = fconfig.apply_pd( df);
    
    #Evaluate statistics
    tr_acc = accuracy( df['prediction'], df['labels'] );
    tr_rmse = rmse( df['prediction'], df['labels'] );
    return df,tr_acc,tr_rmse;

"""Predict coordinated for a single line of features"""
def predict_line( feat_line, model, filters  ):
    predictions   = model.predict( feat_line ); 
    probabilities = model.predict_proba( feat_line ); 
    #Filter the results (after this we discard probabilities)
    predictions = filters.apply_np(predictions,probabilities,None,None);    
    return predictions;    

def predict_image(image, models, line_idx, displacements, r_max, n_x, filters, w_avg=None, w_std=None ):
    #First extract the feature vectors
    fvec,pvec,ivec  = utils.extract_image( image, line_idx, displacements, r_max, n_x );

    if w_avg is not None and w_std is not None:
        fvec = (fvec-w_avg)/w_std;

    #Then predict the image line-by-line    
    line_predictions = [];
    for lidx in range( len(line_idx)):
        predpos = predict_line( fvec[lidx,:,:], models[lidx], filters );
        line_predictions.append(predpos);
        
    #Re-assemble the image and filter it
    predicted_idx = np.array( line_predictions );
    predicted_idx = filters.apply_np(predicted_idx,None,pvec,ivec);    
    #Transfer to displacement
    disp_pred = pvec - predicted_idx;
    disp_pred[predicted_idx<1] = -1;
    return disp_pred;
    

def pop_examples( models, sample_names, data_path, line_idx, disp_idx, px_radi, n_x, filters, save_path="../images", do_write=False):
    try:
        os.mkdir( save_path );
    except:
        pass;

    sample_names  = sample_names.reset_index(drop=True);
    sample_images = utils.read_images( sample_names, data_path );
    for iidx,image in enumerate(sample_images):
        predimg = predict_image(image,models,line_idx, disp_idx, px_radi, n_x-2*px_radi, filters );
        
        plt.figure(1)
        plt.imshow( image, cmap='gray' );
        plt.clim(0,255)
        plt.show(block=False);
        plt.pause(0.1);
        plt.figure(2)
        predimg = cv2.resize(predimg,(int(2.5*predimg.shape[0]),2*predimg.shape[0]) );
        plt.imshow( predimg, cmap='magma' );
        plt.clim(0,90)
        plt.show(block=False);
        plt.pause(0.1);
        
        if do_write:
            filename = "%s/%s.png" % (save_path,sample_names.loc[iidx]['hash']);
            cv2.imwrite(filename, image );
            filename = "%s/%s_DP.png" % (save_path,sample_names.loc[iidx]['hash']);
            cv2.imwrite(filename, predimg );
        
        
    








