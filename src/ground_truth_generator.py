# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

import utilities as utils

refr_path = "../ads/reference_pattern.png";
data_path = "../ads/IR";
save_path = "../ads/GT";

max_displacement = 128;
block_size = 21;

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""Make sure output directory exists"""
try:
    os.mkdir( save_path );
except:
    pass;

"""Read reference image"""
imgREF = cv2.imread( refr_path, 0 );
"""Get list of IR image filenames"""
imglist = utils.list_images( data_path );

"""Set up stereo BM"""
stereo = cv2.StereoBM_create( numDisparities=max_displacement, blockSize=block_size );
stereo.setMinDisparity(0);
#stereo.setPreFilterCap(33);
#stereo.setSpeckleWindowSize(0);
#stereo.setSpeckleRange(2);
#stereo.setDisp12MaxDiff(2)

"""Loop over all IR images to generate the disparity map"""
for idx,entr in imglist.iterrows():
    print( idx )
    imgMEAS = cv2.imread( entr['filename'], 0 );
    disparity_map = stereo.compute( imgMEAS, imgREF );
    
    """Save 16 bit disparity map to file"""
    disparity_map[disparity_map<0]=0;
    savefile_name = "%s/%s.png" % (save_path,entr['hash']);
    cv2.imwrite(savefile_name, disparity_map.astype(np.uint16) );
    
    """Ocassionally plot the image and disparity map"""
    if idx%30==0:
        plt.figure(1)
        plt.imshow( imgMEAS )
        plt.show(block=False )
        plt.pause(0.02)
    
        plt.figure(2)
        plt.imshow( disparity_map/16.0 )
        plt.clim(-16,256/2)
        plt.show(block=False )
        plt.pause(0.1)
